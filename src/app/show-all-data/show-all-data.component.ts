import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-show-all-data',
  templateUrl: './show-all-data.component.html',
  styleUrls: ['./show-all-data.component.css']
})
export class ShowAllDataComponent implements OnInit {

  studentData: any = [];

  constructor(private httpClient: HttpClient) { 

    this.httpClient.get("http://localhost:55893/Api/StudentData").subscribe(data => this.studentData = data);
  };

  deleteData(id: any){
    this.httpClient.get("http://localhost:55893/Api/Delete/"+id);
  }

  ngOnInit() {
  }

}
