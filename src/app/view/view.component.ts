import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap} from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

studentDetailId: any;

studentData: any = {};

  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient) {
this.activatedRoute.paramMap.subscribe(param =>{
  this.studentDetailId = param.get("studentId");
  this.httpClient.get("http://localhost:55893/Api/StudentData/"+ this.studentDetailId).subscribe(data => this.studentData = data);
}

);
   }

  ngOnInit() {
  }

}
