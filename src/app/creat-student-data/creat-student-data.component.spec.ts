import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatStudentDataComponent } from './creat-student-data.component';

describe('CreatStudentDataComponent', () => {
  let component: CreatStudentDataComponent;
  let fixture: ComponentFixture<CreatStudentDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatStudentDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatStudentDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
