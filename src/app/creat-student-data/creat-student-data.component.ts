import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-creat-student-data',
  templateUrl: './creat-student-data.component.html',
  styleUrls: ['./creat-student-data.component.css']
})
export class CreatStudentDataComponent implements OnInit {

  studentData: any = {
    name: "",
    fathersName: "",
    mothersName: "",
    village: "",
    mobileNumber: ""
  };

  constructor(private httpClient: HttpClient) {
  
  }

  saveData() {
    this.httpClient.post("http://localhost:55893/Api/StudentData", this.studentData).subscribe(data => {
      if (data == true) {
        alert("Data added successfully");
      }
      else {
        alert("Error");
      }
    }
    )
  };

  ngOnInit() {
  }

}
