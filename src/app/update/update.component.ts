import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  studentUpdateId: any;
  studentData: any = [];
  constructor(private activatedRoute: ActivatedRoute, private httpClient: HttpClient) {
    this.activatedRoute.paramMap.subscribe(param => {
      this.studentUpdateId = param.get("studentId");
      this.httpClient.get("http://localhost:55893/Api/StudentData/" + this.studentUpdateId).subscribe(data => this.studentData = data);
    });
  }

  ngOnInit() {
  }

}
