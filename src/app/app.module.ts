import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { CreatStudentDataComponent } from './creat-student-data/creat-student-data.component';
import { ShowAllDataComponent } from './show-all-data/show-all-data.component';
import { ViewComponent } from './view/view.component';

import { UpdateComponent } from './update/update.component';


@NgModule({
  declarations: [
    AppComponent,
    CreatStudentDataComponent,
    ShowAllDataComponent,
    ViewComponent,

    UpdateComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: 'creat', component: CreatStudentDataComponent },
      { path: 'showData', component: ShowAllDataComponent },
      { path: 'idDetail/:studentId', component: ViewComponent },

      { path: 'idUpdate/:studentId', component: UpdateComponent }
    ]),
    FormsModule,
    HttpClientModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
